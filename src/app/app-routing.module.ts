import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ListarComponent } from "./components/listar/listar.component";
import { RegistroPersonaComponent } from "./components/registro-persona/registro-persona.component";

const routes: Routes = [
  { path: "registro-persona", component: RegistroPersonaComponent },
  { path: "listar", component: ListarComponent },
  { path: "home", component: HomeComponent },
  { path: "**", pathMatch: "full", redirectTo: "home" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
